package com.example.app2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        // Recogemos el intent que nos ha invocado
        Intent i = this.getIntent();
        // Obtenemos el mensaje que hemos pasado
        String mensajeRecibido = i.getStringExtra("mensajeActividad");
        // Mostramos el mensaje en el Log
        Log.i(this.getLocalClassName(),mensajeRecibido);
    }
}